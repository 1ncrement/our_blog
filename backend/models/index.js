/**
 * Created by Increment on 20.11.2016.
 */
const
	mg = require('mongoose'),
	config = require('../config');

mg.Promise = global.Promise;//костыль

mg.connect(config.mongoose.uri);

module.exports = mg;