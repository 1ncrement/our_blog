/**
 * Created by Increment on 20.11.2016.
 */
const
	mg = require('./index'),
	bcrypt = require('bcryptjs'),

	UserSchema = new mg.Schema({
		username: {
			type: String,
			index: true,
			unique: true,
			required: true
		},
		password: {
			type: String,
			required: true
		}
	});

let User = module.exports = mg.model('users', UserSchema);

module.exports.createUser = (newUser, callback)=>{
	bcrypt.genSalt(10, (err, salt)=>{
		bcrypt.hash(newUser.password, salt, (err, hash)=>{
			newUser.password = hash;
			newUser.save(callback);
		})
	})
};

module.exports.getUserById = (id, callback)=>{
	User.findById(id, callback);
};

module.exports.getUserByUserName = (username, callback)=>{
	let query = {username: username};
	User.findOne(query, callback);
};

module.exports.comparePassword = (candidatePassword, hash, callback) =>{
	bcrypt.compare(candidatePassword, hash, (err, isMatch)=> {
		if(err) throw err;
		callback(null, isMatch);
	});
};