/**
 * Created by Increment on 20.11.2016.
 */
const
	userCtrl = {},
	User = require('../models/user'),
	express = require('express'),
	app = express(),
	passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy;


/** POST */
userCtrl.registerUser = (req, res, next) => {
	let
		b = req.body,
		newUser = new User({
			username: b.username,
			password: b.password
		});

	User.createUser(newUser, (err, user)=>{
		if(err) throw err;
		res.json(user);
	});

	// .save()
	// .then((user)=>{res.json(user)})
	// .catch((err)=>{console.error(err); next(err)});
};
userCtrl.loginUser = (req, res, next)=> {
	res.json('login user, success');
};


/** GET */
userCtrl.logout = (req, res, next)=> {
	req.logout();
	res.json('you are logged out');
};
userCtrl.getUsers = (req, res, next) => {
	User.find({})
		.then((user)=>{res.json(user)})
		.catch((err)=>{console.error(err); next(err)});
};
userCtrl.removeUsers = (req, res, next) => {
	User.remove({})
		.then((user)=>{res.json(user)})
		.catch((err)=>{console.error(err); next(err)});
};

module.exports = userCtrl;