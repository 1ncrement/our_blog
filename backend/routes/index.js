/**
 * Created by Increment on 20.11.2016.
 */
const
	express = require('express'),
	path = require('path'),
	router = express.Router(),
	usersCtrl = require('../controllers/users'),
	passport = require('passport'),
	User = require('../models/user'),
	LocalStrategy = require('passport-local').Strategy,
	canLogged = require('../middlewares/can-logged')
	;

/** Passport */
passport.use(new LocalStrategy(function(username, password, done) {
		User.getUserByUserName(username, (err, user)=>{
			if(err) throw err;
			if(!user){
				return done(null, false, {message: 'Unknow user'});
			}

			User.comparePassword(password, user.password, (err, isMatch)=>{
				if(isMatch){
					return done(null, user);
				}
				else{
					return done(null, false, {message: 'Invalid password'});
				}
			})
		});
	}));
passport.serializeUser((user, done)=> {
	done(null, user.id);
});
passport.deserializeUser((id, done)=> {
	User.getUserById(id, (err, user)=> {
		if(err) throw err;
		done(err, user);
	});
});

/** User post */
router.post('/register', usersCtrl.registerUser);
router.post('/login', canLogged('local', {
	seccessRedirect: '/',
	failureRedirect: '/out',
	failureFlash: true
}), usersCtrl.loginUser);

/** User get */
router.get('/get-users', usersCtrl.getUsers);
router.get('/remove-users', usersCtrl.removeUsers);
router.get('/logout', usersCtrl.logout);

module.exports = router;