const
	route = require('./routes'),
	express = require('express'),
	expValid = require('express-validator'),
	session = require('express-session'),
	passport = require('passport'),
	path = require('path'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	configServer = require('./config'),
	app = new express()
	;

/** CORS Expess */
app.use(function(req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
	next();
});

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(session({
	secret: 'SECRET-KEY',
	saveUninitialized: true,
	resave: true
}));

/** Passport */
app.use(passport.initialize());
app.use(passport.session());

/** Validator */
app.use(expValid({
	errorFormatter: function (param, msg, value) {
		let
			namespace = param.split('.'),
			root = namespace.shift(),
			formParam = root;

		while(namespace.length){
			formParam += `[${namespace.shift()}]`;
		}

		return {
			param: formParam,
			msg,
			value
		}
	}
}));

/** Routes */
app.use(route);

/** Last handler */
app.use((err, req, res, next) => {
	res.status(err.status || 500).json({
		error: {
			status: err.status,
			message: err.message
		}
	});
});

app.listen(configServer.server.port, ()=> {
	console.info(`Listening on port ${configServer.server.port} http://localhost:${configServer.server.port}/`);
});